package megaman2;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame
{
    Image playerSprite = null;
    Input inputSignal;
    
    public MainGame(String gameName)
    {
	super(gameName);
    }

    @Override
    public void init(GameContainer gc) throws SlickException
    {
	playerSprite = new Image("assets/mm_idle.png");
	
	gc.getInput();
	inputSignal = new Input(0);
	
    }

    @Override
    public void update(GameContainer gc, int i) throws SlickException
    {
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException
    {
	g.drawString("Howdy!", 100, 100);
	
	playerSprite.draw(250,200);
	playerSprite.draw(200,200);
	playerSprite.draw(250,250);
	playerSprite.draw(200,250);
	
	//image      tileset     x  y  w    h      posx,posy
	playerSprite.getSubImage(0, 0, 21, 24).draw(150,150);
	
    }

    public static void main(String[] args)
    {
	try
	{
	    AppGameContainer appgc;
	    appgc = new AppGameContainer(new MainGame("Game Name go here"));
	    appgc.setDisplayMode(800, 600, true);
	    appgc.setAlwaysRender(true);
	    appgc.start();
	}
	catch (SlickException ex)
	{
	    Logger.getLogger(MainGame.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
}